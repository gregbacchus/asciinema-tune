# asciinema-tune

To install

```bash
ln ./asciinema-tune /usr/local/bin/asciinema-tune
```

To use

```bash
asciinema-tune my-file.json > tuned-file.json
asciinema play tuned-file.json
```
